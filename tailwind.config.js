const { guessProductionMode } = require("@ngneat/tailwind");

process.env.TAILWIND_MODE = guessProductionMode() ? 'build' : 'watch';

module.exports = {
    prefix: '',
    mode: 'jit',
    purge: {
      content: [
        './src/**/*.{html,ts,css,scss,sass,less,styl}',
      ]
    },
    darkMode: 'class', // or 'media' or 'class'
    theme: {
      extend: {
        colors: {
          'azul': {
              '30': '#EFF6FF',
              '50': '#F0F6FE',
              '100': '#F7F9FD',
              '200': '#A9DBF6',
              '300': '#1491ED',
              '350': '#148EE0',
              '400': '#0067C2',
                   
          },
          'plomo': {
              '50': '#e7e2e2',
              '100': '#F0F0F0',
              '120': '#ADADAD',
              '140': '#FAFAFA',
              '125': '#D8D8D8',
              '130': '#A0A6B4', 
              '135': '#E9E9E9',                      
              '150': '#F5F3F1',
              '170': '#4F4E4E',
              '180': '#ECECEC',
              '200': '#F6F6F6',
              '230': '#8A8A8A',
              '250': '#C1C6CA',
              '270': '#5D5D5D',
              '280': '#616161',
              '290': '#424242',
              '295': '#4F4E4E',
              '300': '#BCBCBC',
              '310': '#E9E9E9',
              '320': '#646464',
              '325': '#424242',
              '330': '#959595',
              '350': '#8E8E8E',
              '370': '#4A4A4A',
              '400': '#707070',
              '430': '#717070',
              '440': '#535353',
              '450': '#7A7A7A',
              '460': '#9F9F9F',
              '470': '#616161',
              '480': '#BEBEBE',
              '490': '#5B5B5B',
              '500': '#3E3E3E',
              '520': '#575757',
              '800': '#323232',
              '900': '#262626',
                 
                              
          },

          'rojo':{
            '500': '#ED0000',
            '550': '#E03333'
          },

          'naranja':{
            '500': '#F38C52'
          }




        },

        fontFamily: {
          Roboto: ['Roboto'],
          Poppins: ['Poppins'],
          Manrope: ['Manrope']
        }
      },
    },
    variants: {
      extend: {},
    },
    plugins: [require('@tailwindcss/aspect-ratio'),require('@tailwindcss/forms'),require('@tailwindcss/line-clamp'),require('@tailwindcss/typography')],
};
