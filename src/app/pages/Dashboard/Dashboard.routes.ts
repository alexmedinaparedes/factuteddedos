import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { DashboardComponent } from './dashboard.component';
import { VentaComponent } from './venta/venta.component';
import { AlmacenComponent } from './almacen/almacen.component';

const routes: Routes = [
  {
    path: '',
    component: DashboardComponent,

    children: 
    [
        { path: 'venta', component: VentaComponent },
        {path:'almacen',component:AlmacenComponent},

        // redireccionar de   una  ruta  hija ruta  pricipal por defecto
        {path: '', redirectTo: '/login' , pathMatch: 'full'},
    
      ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DashboardRoutingModule {}
