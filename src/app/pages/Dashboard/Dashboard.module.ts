import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './dashboard.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { RouterModule } from '@angular/router';
import { VentaComponent } from './venta/venta.component';
import { AlmacenComponent } from './almacen/almacen.component';




@NgModule({
  declarations: [
    DashboardComponent,
    VentaComponent,
    AlmacenComponent
  
  ],

  
  imports: [
    CommonModule,
    SharedModule,
    RouterModule
  ]
})
export class DashboardModule { }
