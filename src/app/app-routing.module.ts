import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthRoutingModule } from './auth/auth.routes';
import { DashboardRoutingModule } from './pages/Dashboard/Dashboard.routes';




const routes: Routes = [
  

  {path: '', redirectTo: 'login' , pathMatch: 'full'},

];

@NgModule({
  imports: [RouterModule.forRoot(routes,{useHash:true}),
            AuthRoutingModule,
            DashboardRoutingModule
        
          ],

  exports: [RouterModule]
})
export class AppRoutingModule { }
