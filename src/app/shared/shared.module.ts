import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavbarComponent } from './navbar/navbar.component';
import { FooterComponent } from './footer/footer.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { NavbarprincipalComponent } from './navbarprincipal/navbarprincipal.component';
import { RouterModule } from '@angular/router';



@NgModule({
  declarations: [
    NavbarComponent,
    FooterComponent,
    SidebarComponent,
    NavbarprincipalComponent
  ],
  exports: [
    NavbarComponent,
    FooterComponent,
    SidebarComponent,
    NavbarprincipalComponent
  ],

  imports: [
    CommonModule, 
    RouterModule
  ]
})
export class SharedModule { }
